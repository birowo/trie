package trie

const ndsMax = 128

type (
	Node[T any] struct {
		Sgmn string
		Chr  byte
		Val  T
		Nds  *Nodes[T]
	}
	Nodes[T any] [ndsMax]Node[T]
)

func (nds *Nodes[T]) Insert(str string, val T) {
	strLen := len(str)
	if strLen == 0 {
		return
	}
	idx := 0
	for {
		chr := str[idx]
		if chr < ndsMax && nds[chr].Chr == 0 {
			nds[chr] = Node[T]{
				str[idx+1:],
				chr,
				val,
				nil,
			}
			return
		}
		nd := nds[chr]
		idx++
		i := 0
		sgmnLen := len(nd.Sgmn)
		for idx < strLen && i < sgmnLen && str[idx] == nd.Sgmn[i] {
			idx++
			i++
		}
		if i < sgmnLen {
			var nds__ Nodes[T]
			if idx == strLen {
				chr_ := nd.Sgmn[i]
				if chr_ < ndsMax {
					nds__[chr_] = Node[T]{
						nd.Sgmn[i+1:],
						chr_,
						nd.Val,
						nds[chr].Nds,
					}
					nds[chr].Nds = &nds__
					nds[chr].Sgmn = nd.Sgmn[:i]
					nds[chr].Val = val
				}
				return
			}
			chr1 := nds[chr].Sgmn[i]
			chr2 := str[idx]
			if chr1 < ndsMax && chr2 < ndsMax {
				nds__[chr1] = Node[T]{
					nd.Sgmn[i+1:],
					chr1,
					nd.Val,
					nd.Nds,
				}
				nds__[chr2] = Node[T]{
					str[idx+1:],
					chr2,
					val,
					nil,
				}
				nds[chr].Nds = &nds__
				nds[chr].Sgmn = nd.Sgmn[:i]
				var val T
				nds[chr].Val = val
			}
			return
		}
		if idx == strLen {
			return
		}
		if nd.Nds == nil {
			nds[chr].Nds = &Nodes[T]{}
		}
		nds = nds[chr].Nds
	}
}
func (nds *Nodes[T]) Get(str string) T {
	strLen := len(str)
	idx := 0
	for idx < strLen && nds != nil {
		chr := str[idx]
		if chr < ndsMax && nds[chr].Chr != 0 {
			idx++
			nd := nds[chr]
			idx_ := idx + len(nd.Sgmn)
			if idx_ <= strLen && str[idx:idx_] == nd.Sgmn {
				if idx_ == strLen {
					return nd.Val
				}
				idx = idx_
				nds = nd.Nds
			}
		}
	}
	var val T
	return val
}

var idx = 0

func Traverse[T any](nds Nodes[T], cb func(Node[T], int)) { //DFS
	for _, nd := range nds {
		if nd.Chr != 0 {
			cb(nd, idx)
			if nd.Nds != nil {
				idx++
				Traverse[T](*nd.Nds, cb)
				idx--
			}
		}
	}
}
