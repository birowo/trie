package main

import (
	"encoding/json"
	"regexp"
	"strings"

	"gitlab.com/birowo/trie"
)

func main() {
	nds := &trie.Nodes[int]{}
	nds.Insert("abc", 123)
	nds.Insert("bcd", 234)
	nds.Insert("acd", 345)
	nds.Insert("bc", 456)
	nds.Insert("bce", 567)
	nds.Insert("abcde", 678)
	bs, _ := json.MarshalIndent(nds, "", "  ")
	re := regexp.MustCompile(`,*\s*\{\s*"Sgmn": "",\s*"Chr": 0,\s*"Val": 0,\s*"Nds": null\s*\},*`)
	bs = re.ReplaceAll(bs, []byte(""))
	println(string(bs))
	bcd := "bcd"
	println(bcd+":", nds.Get(bcd))
	abcde := "abcde"
	println(abcde+":", nds.Get(abcde))
	var strs []string
	idx_ := -1
	trie.Traverse[int](*nds, func(nd trie.Node[int], idx int) {
		if idx > idx_ {
			strs = append(strs, string(nd.Chr)+nd.Sgmn)
			idx_ = idx
		} else {
			strs[idx] = string(nd.Chr) + nd.Sgmn
		}
		if nd.Val == 0 {
			return
		}
		println("key:", strings.Join(strs[:idx+1], ""), ", val:", nd.Val)
	})
}
